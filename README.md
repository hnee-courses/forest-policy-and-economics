# Forest Policy and Economics

calculations and modelling from the calsses

## Getting started

Lesson 2 - Compounding and Discounting
Net Present value see 020_Interest_Compound.ipynb

## Get LaTex running in the notebooks
Since this is a pain in Windows a docker container will be build

```shell
 
```

## install dep for markdown latex
pip install -r requirements.txt

## install latex
This is for the rendering of the formulas
```shell
sudo apt-get install texlive-latex-base texlive-fonts-recommended texlive-fonts-extra texlive-latex-extra
```

### Task 3 - Project 1
2022-06-06_Home_Work_Assignments.ipynb

## get slides out of the notebook
https://towardsdatascience.com/jupyter-notebook-to-pdf-in-a-few-lines-3c48d68a7a63


def forest_rent_value(A_u, D: list, c, u, v, i, Z):
    """
    Calculate the the Forest Rent W_r

    :param Au: incomes
    :param D: incomes
    :param c: regeneration cost
    :param u: final cutting age, rotation age
    :param v: annual expenditure
    :param i: interest
    :param Z: Stocking Factor Z, density more means more trees

    :return: the rent for all the area
    """
    D.append(A_u)
    A_u_z = sum(D) * Z

    return (A_u_z - (c + u * v)) / i

area = 164 # hectare
Z = 0.8 # Stocking Factor
A_u = 44675 # value of net income received from the harvested crop at the rotation age,
D = [376, 522, 789, 945, 1136] # value of net income received from pole stand


c = 838 # planting expenditure
v = 43 # annual expenditure
u = 81 # rotation age
i = 0.05 # national interest rate

tract_Size = area / u
print(f"each tract is {round(tract_Size,3)} hectares big. The whole area: {area}ha")

fr = forest_rent_value(A_u=A_u, D=D, c=c, u=u, v=v, i=i, Z=Z)
fr = fr / area
print(f"forest rent for one hectare: {round(fr)}EUR")

fr = round(fr * tract_Size, 2)
print(f"forest rent for tract: {fr}EUR for interest rate {100 * i}%")
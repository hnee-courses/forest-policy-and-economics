FROM python:3.9.13-bullseye

RUN apt-get install texlive-latex-base texlive-fonts-recommended texlive-fonts-extra texlive-latex-extra

WORKDIR /app
COPY requirements.txt ./

RUN pip install -r ./requirements.txt